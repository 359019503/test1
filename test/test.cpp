﻿// test.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>

class Base1 {
public:
	Base1() {

	}
	virtual ~Base1() {
		std::cout << "123" << std::endl;
	}
	virtual void test() {
		std::cout << "test" << std::endl;
	}
private:
	int m_n;
};

class Base2 {
public:
	Base2() {

	}
	virtual ~Base2() {
		std::cout << "123" << std::endl;
	}
	virtual void test() {
		std::cout << "test" << std::endl;
	}
private:
	int m_n;
};

class Derive : public  Base1, public Base2 {
public:
	Derive() {

	}
	virtual ~Derive() {
		std::cout << "123" << std::endl;
	}
	virtual void test() {
		std::cout << "Derive::test" << std::endl;
	}
private:
	int m_n;
};

struct A
{
	int ax{1};
	virtual void f0() {}
	virtual void bar() {}
};

struct B : virtual public A           
{                                     
	int bx{2};                        
	void f0() override {}  
	virtual void bar1() {}
};                                    
									  
struct C : virtual public A           
{                                     
	int cx{3};                        
	void f0() override {}             
};                                    

struct D : public B, public C
{
	int dx{4};
	void f0() override {}
};

typedef void(*Fun)();

int main()
{
	//Derive* d = new Derive;
	//Fun fun = (Fun)(*( ((int*)(*(int*)d)) - 2));
	//fun();
	D* dd = new D;
	A* a = dd;
	B* b = dd;
	C* c = dd;
	B bb; 
    std::cout << "Hello World!\n";
}
